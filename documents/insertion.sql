SET FOREIGN_KEY_CHECKS=0;
truncate table editions;
truncate table editeurs;
truncate table langues;
truncate table formats;
truncate table lectures;
truncate table users;
truncate table etats;
truncate table livres;
truncate table auteurs_livres;
truncate table auteurs;
truncate table series;
truncate table sexes;
truncate table genres_livres;
truncate table genres;
SET FOREIGN_KEY_CHECKS=1;

INSERT INTO `langues`
(`idLangue`,
`sLangueKey`,
`sLangueAbbrv`)
VALUES
(null,
"key_langues_fr",
"fr");

INSERT INTO `langues`
(`idLangue`,
`sLangueKey`,
`sLangueAbbrv`)
VALUES
(null,
"key_langues_en",
"en");

INSERT INTO `langues`
(`idLangue`,
`sLangueKey`,
`sLangueAbbrv`)
VALUES
(null,
"key_langues_gr",
"en");

INSERT INTO `formats`
(`idFormat`,
`sFormatKey`)
VALUES
(null,
"key_formats_grand");

INSERT INTO `formats`
(`idFormat`,
`sFormatKey`)
VALUES
(null,
"key_formats_poche");

INSERT INTO `formats`
(`idFormat`,
`sFormatKey`)
VALUES
(null,
"key_formats_ebook");

INSERT INTO `formats`
(`idFormat`,
`sFormatKey`)
VALUES
(null,
"key_formats_audio");

INSERT INTO `formats`
(`idFormat`,
`sFormatKey`)
VALUES
(null,
"key_formats_autre");

INSERT INTO `editeurs`
(`idEditeur`,
`sEditeurKey`,
`sLienWebKey`,
`sImgPath`)
VALUES
(null,
"key_editeurs_gallimard",
"key_editeurs_web_gallimard",
"https://upload.wikimedia.org/wikipedia/fr/thumb/e/e6/Gallimard_logo.svg/1280px-Gallimard_logo.svg.png");

INSERT INTO `editeurs`
(`idEditeur`,
`sEditeurKey`,
`sLienWebKey`,
`sImgPath`)
VALUES
(null,
"key_editeurs_tor",
"key_editeurs_web_tor",
"http://www.teleread.com/wp-content/uploads/2013/04/TorRocket.png");

INSERT INTO `editeurs`
(`idEditeur`,
`sEditeurKey`,
`sLienWebKey`,
`sImgPath`)
VALUES
(null,
"key_editeurs_pocket",
"key_editeurs_web_pocket",
"http://livressedeslivres.e-monsite.com/medias/album/pocket.jpg");

INSERT INTO `sexes`
(`idSexe`,
`sSexeKey`)
VALUES
(null,
"key_sexes_homme");

INSERT INTO `sexes`
(`idSexe`,
`sSexeKey`)
VALUES
(null,
"key_sexes_femme");

INSERT INTO `sexes`
(`idSexe`,
`sSexeKey`)
VALUES
(null,
"key_sexes_autre");

INSERT INTO `etats`
(`idEtat`,
`sEtatKey`)
VALUES
(null,
"key_etats_lu");

INSERT INTO `etats`
(`idEtat`,
`sEtatKey`)
VALUES
(null,
"key_etats_a_lire");

INSERT INTO `etats`
(`idEtat`,
`sEtatKey`)
VALUES
(null,
"key_etats_en_lecture");

INSERT INTO `auteurs`
(`idAuteur`,
`sName`,
`dDateNaissance`,
`dDateMort`,
`sLienWeb`,
`sBioKey`,
`fAvgAuteur`,
`sImgPath`)
VALUES
(null,
"Steven Erikson",
"1959-10-07",
null,
"http://www.steven-erikson.com/",
"key_auteurs_bio_steven_erikson",
null,
"https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Steven_Erikson_reading_a_book.jpg/479px-Steven_Erikson_reading_a_book.jpg");

INSERT INTO `auteurs`
(`idAuteur`,
`sName`,
`dDateNaissance`,
`dDateMort`,
`sLienWeb`,
`sBioKey`,
`fAvgAuteur`,
`sImgPath`)
VALUES
(null,
"Bernard Werber",
"1961-09-18",
null,
"http://www.bernardwerber.com/",
"key_auteurs_bio_bernard_werber",
null,
"https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/Bernard_WERBER_par_Michel_Restany.jpg/220px-Bernard_WERBER_par_Michel_Restany.jpg");

INSERT INTO `auteurs`
(`idAuteur`,
`sName`,
`dDateNaissance`,
`dDateMort`,
`sLienWeb`,
`sBioKey`,
`fAvgAuteur`,
`sImgPath`)
VALUES
(null,
"Jules Verne",
"1828-02-08",
"1905-03-24",
null,
"key_auteurs_bio_jules_verne",
null,
"https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/F%C3%A9lix_Nadar_1820-1910_portraits_Jules_Verne.jpg/220px-F%C3%A9lix_Nadar_1820-1910_portraits_Jules_Verne.jpg");

INSERT INTO `auteurs`
(`idAuteur`,
`sName`,
`dDateNaissance`,
`dDateMort`,
`sLienWeb`,
`sBioKey`,
`fAvgAuteur`,
`sImgPath`)
VALUES
(null,
"Friedrich Nietzsche",
"1844-10-15",
"1900-04-25",
null,
"key_auteurs_bio_friedrich_nietzsche",
null,
"https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/Portrait_of_Friedrich_Nietzsche.jpg/260px-Portrait_of_Friedrich_Nietzsche.jpg");

INSERT INTO `auteurs`
(`idAuteur`,
`sName`,
`dDateNaissance`,
`dDateMort`,
`sLienWeb`,
`sBioKey`,
`fAvgAuteur`,
`sImgPath`)
VALUES
(null,
"David Eddings",
"1931-07-07",
"2009-06-02",
null,
"key_auteurs_bio_david_eddings",
null,
"https://upload.wikimedia.org/wikipedia/en/e/ee/David_Eddings_portrait.jpg");

INSERT INTO `auteurs`
(`idAuteur`,
`sName`,
`dDateNaissance`,
`dDateMort`,
`sLienWeb`,
`sBioKey`,
`fAvgAuteur`,
`sImgPath`)
VALUES
(null,
"Leigh Eddings (Judith Leigh Schall)",
"1937-09-30",
"2007-02-28",
null,
"key_auteurs_bio_leigh_eddings",
null,
"C:\somePlaceWithImages\image_leigh_eddings");

INSERT INTO `genres`
(`idGenre`,
`sGenreKey`,
`sGenreDescriptionKey`)
VALUES
(null,
"key_genres_fiction",
"key_genres_desc_fiction");

INSERT INTO `genres`
(`idGenre`,
`sGenreKey`,
`sGenreDescriptionKey`)
VALUES
(null,
"key_genres_science_fiction",
"key_genres_desc_science_fiction");

INSERT INTO `genres`
(`idGenre`,
`sGenreKey`,
`sGenreDescriptionKey`)
VALUES
(null,
"key_genres_fantastique",
"key_genres_desc_fantastique");

INSERT INTO `genres`
(`idGenre`,
`sGenreKey`,
`sGenreDescriptionKey`)
VALUES
(null,
"key_genres_suspense",
"key_genres_desc_suspense");

INSERT INTO `genres`
(`idGenre`,
`sGenreKey`,
`sGenreDescriptionKey`)
VALUES
(null,
"key_genres_policier",
"key_genres_desc_policier");

INSERT INTO `genres`
(`idGenre`,
`sGenreKey`,
`sGenreDescriptionKey`)
VALUES
(null,
"key_genres_biographie",
"key_genres_desc_biographie");

INSERT INTO `genres`
(`idGenre`,
`sGenreKey`,
`sGenreDescriptionKey`)
VALUES
(null,
"key_genres_autobiographie",
"key_genres_desc_autobiographie");

INSERT INTO `genres`
(`idGenre`,
`sGenreKey`,
`sGenreDescriptionKey`)
VALUES
(null,
"key_genres_historique",
"key_genres_desc_historique");

INSERT INTO `genres`
(`idGenre`,
`sGenreKey`,
`sGenreDescriptionKey`)
VALUES
(null,
"key_genres_histoire",
"key_genres_desc_histoire");

INSERT INTO `genres`
(`idGenre`,
`sGenreKey`,
`sGenreDescriptionKey`)
VALUES
(null,
"key_genres_philosophie",
"key_genres_desc_philosophie");

INSERT INTO `livres`
(`idLivre`,
`Serie_idSerie`,
`fAvgRating`)
VALUES
(null,
null,
null);

INSERT INTO `genres_livres`
(`idGenreLivre`,
`Genre_idGenre`,
`Livre_idLivre`)
VALUES
(null,
3,
1);

INSERT INTO `livres`
(`idLivre`,
`Serie_idSerie`,
`fAvgRating`)
VALUES
(null,
null,
null);

INSERT INTO `genres_livres`
(`idGenreLivre`,
`Genre_idGenre`,
`Livre_idLivre`)
VALUES
(null,
1,
2);

INSERT INTO `livres`
(`idLivre`,
`Serie_idSerie`,
`fAvgRating`)
VALUES
(null,
null,
null);

INSERT INTO `genres_livres`
(`idGenreLivre`,
`Genre_idGenre`,
`Livre_idLivre`)
VALUES
(null,
1,
3);

INSERT INTO `livres`
(`idLivre`,
`Serie_idSerie`,
`fAvgRating`)
VALUES
(null,
null,
null);

INSERT INTO `genres_livres`
(`idGenreLivre`,
`Genre_idGenre`,
`Livre_idLivre`)
VALUES
(null,
2,
4);

INSERT INTO `livres`
(`idLivre`,
`Serie_idSerie`,
`fAvgRating`)
VALUES
(null,
null,
null);

INSERT INTO `genres_livres`
(`idGenreLivre`,
`Genre_idGenre`,
`Livre_idLivre`)
VALUES
(null,
10,
5);

INSERT INTO `livres`
(`idLivre`,
`Serie_idSerie`,
`fAvgRating`)
VALUES
(null,
null,
null);

INSERT INTO `genres_livres`
(`idGenreLivre`,
`Genre_idGenre`,
`Livre_idLivre`)
VALUES
(null,
3,
6);

INSERT INTO `auteurs_livres`
(`idAuteurLivre`,
`Livre_idLivre`,
`Auteur_idAuteur`)
VALUES
(null,
1,
1);

INSERT INTO `editions`
(`idEdition`,
`Livre_idLivre`,
`Langue_idLangue`,
`Format_idFormat`,
`Editeur_idEditeur`,
`sTitre`,
`sResume`,
`sISBN`,
`sImgPath`,
`dDateEdition`)
VALUES
(null,
1,
2,
1,
2,
"Toll the Hounds",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique lacus id nulla venenatis, ut molestie tellus consequat. Mauris egestas turpis vitae mi scelerisque egestas. Vestibulum feugiat egestas ex, sit amet feugiat purus molestie ut. Vestibulum euismod non diam vitae malesuada. Aenean imperdiet quam eu ullamcorper venenatis. Nam non nisl vel nibh elementum interdum nec quis neque. Ut venenatis vulputate leo at ultrices. Aliquam et elit quis justo tincidunt interdum non sed purus. Donec hendrerit urna dolor, a ornare quam tristique quis. Duis eu interdum libero. Nulla est lacus, sodales eu condimentum nec, tincidunt vitae risus.

Sed quis mollis lacus. Sed feugiat consectetur orci a eleifend. Praesent dapibus tellus dolor, vitae facilisis tellus ullamcorper ut. Sed rutrum non quam eget rhoncus. Nam turpis lorem, lacinia dapibus lectus eu, sollicitudin cursus tellus. Fusce ac mi ut est facilisis tristique. Fusce pulvinar, lectus et varius vehicula, lorem turpis mollis nisl, pellentesque vestibulum turpis mi ac augue. Phasellus eget diam cursus, tincidunt nisi nec, sodales nisi. Nullam sed tristique ex.",
"978-0-7653-4885-2",
"http://www.elbakin.net/fantasy/modules/public/images/livres/livre-toll-the-hounds-325-9.jpg",
"2009-04-01");

INSERT INTO `editions`
(`idEdition`,
`Livre_idLivre`,
`Langue_idLangue`,
`Format_idFormat`,
`Editeur_idEditeur`,
`sTitre`,
`sResume`,
`sISBN`,
`sImgPath`,
`dDateEdition`)
VALUES
(null,
1,
2,
2,
2,
"Toll the Hounds",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique lacus id nulla venenatis, ut molestie tellus consequat. Mauris egestas turpis vitae mi scelerisque egestas. Vestibulum feugiat egestas ex, sit amet feugiat purus molestie ut. Vestibulum euismod non diam vitae malesuada. Aenean imperdiet quam eu ullamcorper venenatis. Nam non nisl vel nibh elementum interdum nec quis neque. Ut venenatis vulputate leo at ultrices. Aliquam et elit quis justo tincidunt interdum non sed purus. Donec hendrerit urna dolor, a ornare quam tristique quis. Duis eu interdum libero. Nulla est lacus, sodales eu condimentum nec, tincidunt vitae risus.

Sed quis mollis lacus. Sed feugiat consectetur orci a eleifend. Praesent dapibus tellus dolor, vitae facilisis tellus ullamcorper ut. Sed rutrum non quam eget rhoncus. Nam turpis lorem, lacinia dapibus lectus eu, sollicitudin cursus tellus. Fusce ac mi ut est facilisis tristique. Fusce pulvinar, lectus et varius vehicula, lorem turpis mollis nisl, pellentesque vestibulum turpis mi ac augue. Phasellus eget diam cursus, tincidunt nisi nec, sodales nisi. Nullam sed tristique ex.",
"978-0-7653-1008-8",
"http://d.gr-assets.com/books/1328313115l/8177049.jpg",
"2008-09-01");

INSERT INTO `auteurs_livres`
(`idAuteurLivre`,
`Livre_idLivre`,
`Auteur_idAuteur`)
VALUES
(null,
2,
2);

INSERT INTO `editions`
(`idEdition`,
`Livre_idLivre`,
`Langue_idLangue`,
`Format_idFormat`,
`Editeur_idEditeur`,
`sTitre`,
`sResume`,
`sISBN`,
`sImgPath`,
`dDateEdition`)
VALUES
(null,
2,
1,
2,
1,
"Les fourmis",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique lacus id nulla venenatis, ut molestie tellus consequat. Mauris egestas turpis vitae mi scelerisque egestas. Vestibulum feugiat egestas ex, sit amet feugiat purus molestie ut. Vestibulum euismod non diam vitae malesuada. Aenean imperdiet quam eu ullamcorper venenatis. Nam non nisl vel nibh elementum interdum nec quis neque. Ut venenatis vulputate leo at ultrices. Aliquam et elit quis justo tincidunt interdum non sed purus. Donec hendrerit urna dolor, a ornare quam tristique quis. Duis eu interdum libero. Nulla est lacus, sodales eu condimentum nec, tincidunt vitae risus.

Sed quis mollis lacus. Sed feugiat consectetur orci a eleifend. Praesent dapibus tellus dolor, vitae facilisis tellus ullamcorper ut. Sed rutrum non quam eget rhoncus. Nam turpis lorem, lacinia dapibus lectus eu, sollicitudin cursus tellus. Fusce ac mi ut est facilisis tristique. Fusce pulvinar, lectus et varius vehicula, lorem turpis mollis nisl, pellentesque vestibulum turpis mi ac augue. Phasellus eget diam cursus, tincidunt nisi nec, sodales nisi. Nullam sed tristique ex.",
"978-8-4014-3831-8",
"http://i40.servimg.com/u/f40/12/60/98/99/les_fo10.jpg",
"1991-03-14");

INSERT INTO `auteurs_livres`
(`idAuteurLivre`,
`Livre_idLivre`,
`Auteur_idAuteur`)
VALUES
(null,
3,
2);

INSERT INTO `editions`
(`idEdition`,
`Livre_idLivre`,
`Langue_idLangue`,
`Format_idFormat`,
`Editeur_idEditeur`,
`sTitre`,
`sResume`,
`sISBN`,
`sImgPath`,
`dDateEdition`)
VALUES
(null,
3,
1,
2,
3,
"Les thanatonautes",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique lacus id nulla venenatis, ut molestie tellus consequat. Mauris egestas turpis vitae mi scelerisque egestas. Vestibulum feugiat egestas ex, sit amet feugiat purus molestie ut. Vestibulum euismod non diam vitae malesuada. Aenean imperdiet quam eu ullamcorper venenatis. Nam non nisl vel nibh elementum interdum nec quis neque. Ut venenatis vulputate leo at ultrices. Aliquam et elit quis justo tincidunt interdum non sed purus. Donec hendrerit urna dolor, a ornare quam tristique quis. Duis eu interdum libero. Nulla est lacus, sodales eu condimentum nec, tincidunt vitae risus.

Sed quis mollis lacus. Sed feugiat consectetur orci a eleifend. Praesent dapibus tellus dolor, vitae facilisis tellus ullamcorper ut. Sed rutrum non quam eget rhoncus. Nam turpis lorem, lacinia dapibus lectus eu, sollicitudin cursus tellus. Fusce ac mi ut est facilisis tristique. Fusce pulvinar, lectus et varius vehicula, lorem turpis mollis nisl, pellentesque vestibulum turpis mi ac augue. Phasellus eget diam cursus, tincidunt nisi nec, sodales nisi. Nullam sed tristique ex.",
"978-8-9329-0074-2",
"http://media.senscritique.com/media/000000134685/source_big/Les_Thanatonautes.jpg",
"1991-03-14");

INSERT INTO `auteurs_livres`
(`idAuteurLivre`,
`Livre_idLivre`,
`Auteur_idAuteur`)
VALUES
(null,
4,
3);

INSERT INTO `editions`
(`idEdition`,
`Livre_idLivre`,
`Langue_idLangue`,
`Format_idFormat`,
`Editeur_idEditeur`,
`sTitre`,
`sResume`,
`sISBN`,
`sImgPath`,
`dDateEdition`)
VALUES
(null,
4,
1,
2,
3,
"Vingt mille lieues sous les mers",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique lacus id nulla venenatis, ut molestie tellus consequat. Mauris egestas turpis vitae mi scelerisque egestas. Vestibulum feugiat egestas ex, sit amet feugiat purus molestie ut. Vestibulum euismod non diam vitae malesuada. Aenean imperdiet quam eu ullamcorper venenatis. Nam non nisl vel nibh elementum interdum nec quis neque. Ut venenatis vulputate leo at ultrices. Aliquam et elit quis justo tincidunt interdum non sed purus. Donec hendrerit urna dolor, a ornare quam tristique quis. Duis eu interdum libero. Nulla est lacus, sodales eu condimentum nec, tincidunt vitae risus.

Sed quis mollis lacus. Sed feugiat consectetur orci a eleifend. Praesent dapibus tellus dolor, vitae facilisis tellus ullamcorper ut. Sed rutrum non quam eget rhoncus. Nam turpis lorem, lacinia dapibus lectus eu, sollicitudin cursus tellus. Fusce ac mi ut est facilisis tristique. Fusce pulvinar, lectus et varius vehicula, lorem turpis mollis nisl, pellentesque vestibulum turpis mi ac augue. Phasellus eget diam cursus, tincidunt nisi nec, sodales nisi. Nullam sed tristique ex.",
"997-8-1447-4844-62",
"http://www.images-booknode.com/book_cover/4056/full/vingt-mille-lieues-sous-les-mers-4056492.jpg",
"1870-01-01");

INSERT INTO `auteurs_livres`
(`idAuteurLivre`,
`Livre_idLivre`,
`Auteur_idAuteur`)
VALUES
(null,
5,
4);

INSERT INTO `editions`
(`idEdition`,
`Livre_idLivre`,
`Langue_idLangue`,
`Format_idFormat`,
`Editeur_idEditeur`,
`sTitre`,
`sResume`,
`sISBN`,
`sImgPath`,
`dDateEdition`)
VALUES
(null,
5,
3,
2,
3,
"Also sprach Zarathustra",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique lacus id nulla venenatis, ut molestie tellus consequat. Mauris egestas turpis vitae mi scelerisque egestas. Vestibulum feugiat egestas ex, sit amet feugiat purus molestie ut. Vestibulum euismod non diam vitae malesuada. Aenean imperdiet quam eu ullamcorper venenatis. Nam non nisl vel nibh elementum interdum nec quis neque. Ut venenatis vulputate leo at ultrices. Aliquam et elit quis justo tincidunt interdum non sed purus. Donec hendrerit urna dolor, a ornare quam tristique quis. Duis eu interdum libero. Nulla est lacus, sodales eu condimentum nec, tincidunt vitae risus.

Sed quis mollis lacus. Sed feugiat consectetur orci a eleifend. Praesent dapibus tellus dolor, vitae facilisis tellus ullamcorper ut. Sed rutrum non quam eget rhoncus. Nam turpis lorem, lacinia dapibus lectus eu, sollicitudin cursus tellus. Fusce ac mi ut est facilisis tristique. Fusce pulvinar, lectus et varius vehicula, lorem turpis mollis nisl, pellentesque vestibulum turpis mi ac augue. Phasellus eget diam cursus, tincidunt nisi nec, sodales nisi. Nullam sed tristique ex.",
"978-3-9251-0159-5",
"http://ecx.images-amazon.com/images/I/41G5UMOa%2BTL._SX342_BO1,204,203,200_.jpg",
"1885-01-01");

INSERT INTO `editions`
(`idEdition`,
`Livre_idLivre`,
`Langue_idLangue`,
`Format_idFormat`,
`Editeur_idEditeur`,
`sTitre`,
`sResume`,
`sISBN`,
`sImgPath`,
`dDateEdition`)
VALUES
(null,
5,
2,
2,
3,
"Thus Spake Zarathustra",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique lacus id nulla venenatis, ut molestie tellus consequat. Mauris egestas turpis vitae mi scelerisque egestas. Vestibulum feugiat egestas ex, sit amet feugiat purus molestie ut. Vestibulum euismod non diam vitae malesuada. Aenean imperdiet quam eu ullamcorper venenatis. Nam non nisl vel nibh elementum interdum nec quis neque. Ut venenatis vulputate leo at ultrices. Aliquam et elit quis justo tincidunt interdum non sed purus. Donec hendrerit urna dolor, a ornare quam tristique quis. Duis eu interdum libero. Nulla est lacus, sodales eu condimentum nec, tincidunt vitae risus.

Sed quis mollis lacus. Sed feugiat consectetur orci a eleifend. Praesent dapibus tellus dolor, vitae facilisis tellus ullamcorper ut. Sed rutrum non quam eget rhoncus. Nam turpis lorem, lacinia dapibus lectus eu, sollicitudin cursus tellus. Fusce ac mi ut est facilisis tristique. Fusce pulvinar, lectus et varius vehicula, lorem turpis mollis nisl, pellentesque vestibulum turpis mi ac augue. Phasellus eget diam cursus, tincidunt nisi nec, sodales nisi. Nullam sed tristique ex.",
"978-5-6992-0164-8",
"http://data1.whicdn.com/images/14590475/original.jpg",
"1890-01-01");

INSERT INTO `editions`
(`idEdition`,
`Livre_idLivre`,
`Langue_idLangue`,
`Format_idFormat`,
`Editeur_idEditeur`,
`sTitre`,
`sResume`,
`sISBN`,
`sImgPath`,
`dDateEdition`)
VALUES
(null,
5,
2,
2,
3,
"Thus Spoke Zarathustra",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique lacus id nulla venenatis, ut molestie tellus consequat. Mauris egestas turpis vitae mi scelerisque egestas. Vestibulum feugiat egestas ex, sit amet feugiat purus molestie ut. Vestibulum euismod non diam vitae malesuada. Aenean imperdiet quam eu ullamcorper venenatis. Nam non nisl vel nibh elementum interdum nec quis neque. Ut venenatis vulputate leo at ultrices. Aliquam et elit quis justo tincidunt interdum non sed purus. Donec hendrerit urna dolor, a ornare quam tristique quis. Duis eu interdum libero. Nulla est lacus, sodales eu condimentum nec, tincidunt vitae risus.

Sed quis mollis lacus. Sed feugiat consectetur orci a eleifend. Praesent dapibus tellus dolor, vitae facilisis tellus ullamcorper ut. Sed rutrum non quam eget rhoncus. Nam turpis lorem, lacinia dapibus lectus eu, sollicitudin cursus tellus. Fusce ac mi ut est facilisis tristique. Fusce pulvinar, lectus et varius vehicula, lorem turpis mollis nisl, pellentesque vestibulum turpis mi ac augue. Phasellus eget diam cursus, tincidunt nisi nec, sodales nisi. Nullam sed tristique ex.",
"978-8-1813-2001-8",
"http://www.holybooks.com/wp-content/uploads/Thus-Spoke-Zarathustra-by-F.-Nietzsche-ebook-cover.jpg",
"1930-01-01");

INSERT INTO `editions`
(`idEdition`,
`Livre_idLivre`,
`Langue_idLangue`,
`Format_idFormat`,
`Editeur_idEditeur`,
`sTitre`,
`sResume`,
`sISBN`,
`sImgPath`,
`dDateEdition`)
VALUES
(null,
5,
1,
2,
3,
"Ainsi parlait Zarathoustra",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique lacus id nulla venenatis, ut molestie tellus consequat. Mauris egestas turpis vitae mi scelerisque egestas. Vestibulum feugiat egestas ex, sit amet feugiat purus molestie ut. Vestibulum euismod non diam vitae malesuada. Aenean imperdiet quam eu ullamcorper venenatis. Nam non nisl vel nibh elementum interdum nec quis neque. Ut venenatis vulputate leo at ultrices. Aliquam et elit quis justo tincidunt interdum non sed purus. Donec hendrerit urna dolor, a ornare quam tristique quis. Duis eu interdum libero. Nulla est lacus, sodales eu condimentum nec, tincidunt vitae risus.

Sed quis mollis lacus. Sed feugiat consectetur orci a eleifend. Praesent dapibus tellus dolor, vitae facilisis tellus ullamcorper ut. Sed rutrum non quam eget rhoncus. Nam turpis lorem, lacinia dapibus lectus eu, sollicitudin cursus tellus. Fusce ac mi ut est facilisis tristique. Fusce pulvinar, lectus et varius vehicula, lorem turpis mollis nisl, pellentesque vestibulum turpis mi ac augue. Phasellus eget diam cursus, tincidunt nisi nec, sodales nisi. Nullam sed tristique ex.",
"978-8-3615-1636-1",
"http://www.babelio.com/couv/484_1739421.gif",
"1980-01-01");

INSERT INTO `auteurs_livres`
(`idAuteurLivre`,
`Livre_idLivre`,
`Auteur_idAuteur`)
VALUES
(null,
6,
5);

INSERT INTO `auteurs_livres`
(`idAuteurLivre`,
`Livre_idLivre`,
`Auteur_idAuteur`)
VALUES
(null,
6,
6);

INSERT INTO `editions`
(`idEdition`,
`Livre_idLivre`,
`Langue_idLangue`,
`Format_idFormat`,
`Editeur_idEditeur`,
`sTitre`,
`sResume`,
`sISBN`,
`sImgPath`,
`dDateEdition`)
VALUES
(null,
6,
3,
2,
3,
"Polgara la sorcière",
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi tristique lacus id nulla venenatis, ut molestie tellus consequat. Mauris egestas turpis vitae mi scelerisque egestas. Vestibulum feugiat egestas ex, sit amet feugiat purus molestie ut. Vestibulum euismod non diam vitae malesuada. Aenean imperdiet quam eu ullamcorper venenatis. Nam non nisl vel nibh elementum interdum nec quis neque. Ut venenatis vulputate leo at ultrices. Aliquam et elit quis justo tincidunt interdum non sed purus. Donec hendrerit urna dolor, a ornare quam tristique quis. Duis eu interdum libero. Nulla est lacus, sodales eu condimentum nec, tincidunt vitae risus.",
"978-2-2661-7098-7",
"http://www.babelio.com/couv/CVT_Polgara-la-sorciere-Tome-1--Le-temps-des-souffran_2394.jpeg",
"1997-01-01");

INSERT INTO `mybooklist`.`users`
(`idUsers`,
`Langue_idLangue`,
`Sexe_idSexe`,
`username`,
`password`,
`sEmail`)
VALUES
(null,
3,
1,
"NotJohnDoe",
"$2y$10$SlRwtz5Q5JJFFMcW.7gh6OenHkeqnDv3LOBvEDhMivmWoz/BRhdCe",
"email@domain.com");

INSERT INTO `mybooklist`.`users`
(`idUsers`,
`Langue_idLangue`,
`Sexe_idSexe`,
`username`,
`password`,
`sEmail`)
VALUES
(null,
2,
1,
"NotHugo",
"$2y$10$SlRwtz5Q5JJFFMcW.7gh6OenHkeqnDv3LOBvEDhMivmWoz/BRhdCe",
"email@domain.com");

INSERT INTO `mybooklist`.`users`
(`idUsers`,
`Langue_idLangue`,
`Sexe_idSexe`,
`username`,
`password`,
`sEmail`)
VALUES
(null,
2,
3,
"NotGab",
"$2y$10$SlRwtz5Q5JJFFMcW.7gh6OenHkeqnDv3LOBvEDhMivmWoz/BRhdCe",
"email@domain.com");

INSERT INTO `mybooklist`.`lectures`
(`idLecture`,
`Editions_idEdition`,
`Etats_idEtat`,
`Users_idUsers`,
`sCommentaire`,
`iRating`,
`dDateFin`,
`dDateDebut`)
VALUES
(null,
2,
1,
1,
"Incrédible!",
4.4,
null,
null);

INSERT INTO `mybooklist`.`lectures`
(`idLecture`,
`Editions_idEdition`,
`Etats_idEtat`,
`Users_idUsers`,
`sCommentaire`,
`iRating`,
`dDateFin`,
`dDateDebut`)
VALUES
(null,
1,
1,
2,
"Pourri!!1",
1.2,
null,
null);

INSERT INTO `mybooklist`.`lectures`
(`idLecture`,
`Editions_idEdition`,
`Etats_idEtat`,
`Users_idUsers`,
`sCommentaire`,
`iRating`,
`dDateFin`,
`dDateDebut`)
VALUES
(null,
3,
2,
3,
"Je recommande à tous les gens",
5,
"2016-02-02",
"2016-02-10");
