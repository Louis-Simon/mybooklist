<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GenresLivresFixture
 *
 */
class GenresLivresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'idGenreLivre' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'Genre_idGenre' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'Livre_idLivre' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_GenreLivre_Genre1_idx' => ['type' => 'index', 'columns' => ['Genre_idGenre'], 'length' => []],
            'fk_GenreLivre_Livre1_idx' => ['type' => 'index', 'columns' => ['Livre_idLivre'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['idGenreLivre'], 'length' => []],
            'fk_GenreLivre_Genre1' => ['type' => 'foreign', 'columns' => ['Genre_idGenre'], 'references' => ['genres', 'idGenre'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_GenreLivre_Livre1' => ['type' => 'foreign', 'columns' => ['Livre_idLivre'], 'references' => ['livres', 'idLivre'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'idGenreLivre' => 1,
            'Genre_idGenre' => 1,
            'Livre_idLivre' => 1
        ],
    ];
}
