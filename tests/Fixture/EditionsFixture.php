<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EditionsFixture
 *
 */
class EditionsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'idEdition' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'Livre_idLivre' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'Langue_idLangue' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'Format_idFormat' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'Editeur_idEditeur' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sTitre' => ['type' => 'string', 'length' => 150, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'sISBN' => ['type' => 'string', 'length' => 150, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'sImgPath' => ['type' => 'string', 'length' => 1000, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'dDateEdition' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_Edition_Livre_idx' => ['type' => 'index', 'columns' => ['Livre_idLivre'], 'length' => []],
            'fk_Edition_Langue1_idx' => ['type' => 'index', 'columns' => ['Langue_idLangue'], 'length' => []],
            'fk_Edition_Format1_idx' => ['type' => 'index', 'columns' => ['Format_idFormat'], 'length' => []],
            'fk_Edition_Editeur1_idx' => ['type' => 'index', 'columns' => ['Editeur_idEditeur'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['idEdition'], 'length' => []],
            'un_sISBN' => ['type' => 'unique', 'columns' => ['sISBN'], 'length' => []],
            'fk_Edition_Editeur1' => ['type' => 'foreign', 'columns' => ['Editeur_idEditeur'], 'references' => ['editeurs', 'idEditeur'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_Edition_Format1' => ['type' => 'foreign', 'columns' => ['Format_idFormat'], 'references' => ['formats', 'idFormat'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_Edition_Langue1' => ['type' => 'foreign', 'columns' => ['Langue_idLangue'], 'references' => ['langues', 'idLangue'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_Edition_Livre' => ['type' => 'foreign', 'columns' => ['Livre_idLivre'], 'references' => ['livres', 'idLivre'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'idEdition' => 1,
            'Livre_idLivre' => 1,
            'Langue_idLangue' => 1,
            'Format_idFormat' => 1,
            'Editeur_idEditeur' => 1,
            'sTitre' => 'Lorem ipsum dolor sit amet',
            'sISBN' => 'Lorem ipsum dolor sit amet',
            'sImgPath' => 'Lorem ipsum dolor sit amet',
            'dDateEdition' => '2016-03-04'
        ],
    ];
}
