<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LecturesFixture
 *
 */
class LecturesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'idLecture' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'Livre_idLivre' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'Etat_idEtat' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'Membre_idMembre' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sCommentaire' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'iRating' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dDateFin' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'dDateDebut' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_Lecture_Livre1_idx' => ['type' => 'index', 'columns' => ['Livre_idLivre'], 'length' => []],
            'fk_Lecture_Etat1_idx' => ['type' => 'index', 'columns' => ['Etat_idEtat'], 'length' => []],
            'fk_Lecture_Membre1_idx' => ['type' => 'index', 'columns' => ['Membre_idMembre'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['idLecture'], 'length' => []],
            'fk_Lecture_Etat1' => ['type' => 'foreign', 'columns' => ['Etat_idEtat'], 'references' => ['etats', 'idEtat'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_Lecture_Livre1' => ['type' => 'foreign', 'columns' => ['Livre_idLivre'], 'references' => ['livres', 'idLivre'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_Lecture_Membre1' => ['type' => 'foreign', 'columns' => ['Membre_idMembre'], 'references' => ['membres', 'idMembre'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'idLecture' => 1,
            'Livre_idLivre' => 1,
            'Etat_idEtat' => 1,
            'Membre_idMembre' => 1,
            'sCommentaire' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'iRating' => 1,
            'dDateFin' => '2016-03-04',
            'dDateDebut' => '2016-03-04'
        ],
    ];
}
