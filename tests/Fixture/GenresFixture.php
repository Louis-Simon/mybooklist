<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GenresFixture
 *
 */
class GenresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'idGenre' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'sGenreKey' => ['type' => 'string', 'length' => 150, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'sGenreDescriptionKey' => ['type' => 'string', 'length' => 150, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['idGenre'], 'length' => []],
            'un_sGenreKey' => ['type' => 'unique', 'columns' => ['sGenreKey'], 'length' => []],
            'un_sGenreDescriptionKey' => ['type' => 'unique', 'columns' => ['sGenreDescriptionKey'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'idGenre' => 1,
            'sGenreKey' => 'Lorem ipsum dolor sit amet',
            'sGenreDescriptionKey' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
