<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EditeursFixture
 *
 */
class EditeursFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'idEditeur' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'sEditeurKey' => ['type' => 'string', 'length' => 150, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'sLienWebKey' => ['type' => 'string', 'length' => 150, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'sImgPath' => ['type' => 'string', 'length' => 1000, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['idEditeur'], 'length' => []],
            'un_sEditeurKey' => ['type' => 'unique', 'columns' => ['sEditeurKey'], 'length' => []],
            'un_sLienWebKey' => ['type' => 'unique', 'columns' => ['sLienWebKey'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'idEditeur' => 1,
            'sEditeurKey' => 'Lorem ipsum dolor sit amet',
            'sLienWebKey' => 'Lorem ipsum dolor sit amet',
            'sImgPath' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
