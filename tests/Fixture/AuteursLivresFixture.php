<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AuteursLivresFixture
 *
 */
class AuteursLivresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'idAuteurLivre' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'Livre_idLivre' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'Auteur_idAuteur' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_AuteurLivre_Livre1_idx' => ['type' => 'index', 'columns' => ['Livre_idLivre'], 'length' => []],
            'fk_AuteurLivre_Auteur1_idx' => ['type' => 'index', 'columns' => ['Auteur_idAuteur'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['idAuteurLivre'], 'length' => []],
            'fk_AuteurLivre_Auteur1' => ['type' => 'foreign', 'columns' => ['Auteur_idAuteur'], 'references' => ['auteurs', 'idAuteur'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_AuteurLivre_Livre1' => ['type' => 'foreign', 'columns' => ['Livre_idLivre'], 'references' => ['livres', 'idLivre'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'idAuteurLivre' => 1,
            'Livre_idLivre' => 1,
            'Auteur_idAuteur' => 1
        ],
    ];
}
