<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AuteursLivresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AuteursLivresTable Test Case
 */
class AuteursLivresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AuteursLivresTable
     */
    public $AuteursLivres;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.auteurs_livres'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AuteursLivres') ? [] : ['className' => 'App\Model\Table\AuteursLivresTable'];
        $this->AuteursLivres = TableRegistry::get('AuteursLivres', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AuteursLivres);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
