<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GenresLivresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GenresLivresTable Test Case
 */
class GenresLivresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GenresLivresTable
     */
    public $GenresLivres;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.genres_livres'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('GenresLivres') ? [] : ['className' => 'App\Model\Table\GenresLivresTable'];
        $this->GenresLivres = TableRegistry::get('GenresLivres', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GenresLivres);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
