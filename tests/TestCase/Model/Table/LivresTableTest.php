<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LivresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LivresTable Test Case
 */
class LivresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LivresTable
     */
    public $Livres;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.livres',
        'app.auteurs',
        'app.auteurs_livres',
        'app.genres',
        'app.genres_livres'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Livres') ? [] : ['className' => 'App\Model\Table\LivresTable'];
        $this->Livres = TableRegistry::get('Livres', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Livres);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
