<?php
// src/Controller/AuteursController.php
namespace App\Controller;

use Cake\Event\Event;
class AuteursController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'display']);
    }

    public function index()
    {
        $auteurs = $this->Auteurs->find('all');
        $this->set(compact('auteurs'));
    }

    public function add()
    {
        $auteur = $this->Auteurs->newEntity();
        if ($this->request->is('auteur')){
            $post = $this->Auteurs->patchEntity($auteur, $this->request->data);
            if($this->Auteurs->save($auteur)){
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('auteur', $auteur);
    }

    public function edit($id = null)
    {
        $auteur = $this->Auteurs->get($id);
        if($this->request->is(['auteur', 'put'])){
            $auteur = $this->Auteurs->patchEntity($auteur, $this->request->data);
            if($this->Auteurs->save($auteur)){
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set('auteur', $auteur);
    }

    public function display($id)
    {
        $this->loadModel('Editions');
        $this->loadModel('Genres');
        $genresId = array();
        $auteur = $this->Auteurs->get($id, ['contain'=>['Livres' => ['Genres']]]);
        if($this->request->is(['auteur', 'put'])){
            $auteur = $this->Auteurs->patchEntity($auteur, $this->request->data);
        }
        foreach($auteur->livres as $livre){
            $parameters = array(
                'conditions' => array(
                    'Livre_idLivre' => $livre->idLivre
                )
            );

            $editions = $this->Editions->find('all', $parameters);
            foreach($editions as $edition){
                $parameters = array(
                    'conditions' => array(
                        'Livre_idLivre' => $edition->Livre_idLivre
                    )
                );
                $dateOriginale = $this->Editions->find('all', $parameters)->select([$editions->func()->min('dDateEdition')]);

                $parameters = array(
                    'conditions' => array(
                        'Livre_idLivre' => $edition->Livre_idLivre,
                        'OR' =>[['Langue_idLangue' => 1 ], ['dDateEdition' => $dateOriginale]]
                    ),
                    'order' => ['dDateEdition' => 'DESC']
                );
                $livre->edition = $this->Editions->find('all', $parameters)->first();
            }


            foreach($livre->genres as $genre){
                array_push($genresId, $genre->idGenre);
            }

        }
        $genresId = array_unique($genresId);

        $parameters = array(
            'conditions' => array(
                'idGenre IN' => $genresId
            )
        );
        $auteur->genres = $this->Genres->find('all', $parameters);

        $this->set('auteur', $auteur);
    }

    public function delete($id)
    {
        $this->request->allowMethod(['auteur', 'delete']);
        $auteur = $this->Auteurs->get($id);
        return $this->redirect(['action' => 'index']);
    }
}
