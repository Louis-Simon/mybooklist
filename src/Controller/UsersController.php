<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\I18n;

class UsersController extends AppController {

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        $this->Auth->allow(['signup', 'logout', 'login']);
    }

    public function initialize() {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        //$this->RequestHandler->config('inputTypeMap.json', ['json_decode', true]);
    }

    public function index() {
        $this->set('users', $this->Users->find('all'));
    }

    public function view($id) {
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    public function signup() {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['controller' => 'editions','action' => 'index']);
            }
            $this->Flash->error(__('Unable to add the user.'));
        }
        $this->set('user', $user);
    }

    public function login() {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);    
                $user = $this->Users->get($user['idUsers'], ['contain' => 'Langues']);  
                $this->request->session()->write('Config.language', $user->langue->sLangueAbbrv);
//                I18n::locale($user->langue->sLangueAbbrv);
            } else {
                throw new \Cake\Network\Exception\InternalErrorException(__('Invalid username or password, try again'));
            }
        }
    }

    public function logout() {
        return $this->redirect($this->Auth->logout());
    }

}
