<?php
// src/Controller/LecturesController.php
namespace App\Controller;
use Cake\Event\Event;
class LecturesController extends AppController
{
    public function initialize(){
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->deny();
    }
    public function index()
    {
        $lectures = $this->Lectures->find('all');
        $this->set(compact('lectures'));
    }
    public function display(){
        $idUser = $this->Auth->user('idUsers');
        $parameters = array(
            'conditions' => array(
                'Users_idUsers' => $idUser
            )
        );
        $lectures = $this->Lectures->find('all', $parameters)->contain(['Editions'=>['Editeurs', 'Livres'=>['Auteurs'], 'Langues', 'Formats'], 'Etats']);
        $this->set(compact('lectures'));
    }

    public function add($idEdition)
    {
        $this->autoRender = false;
        $lecture = $this->Lectures->newEntity();

        $lecture->Editions_idEdition = $idEdition;
        $lecture->Etats_idEtat = 2;
        $lecture->Users_idUsers = $this->Auth->user('idUsers');
        if(!$this->Lectures->save($lecture)){
            throw new \Cake\Network\Exception\InternalErrorException(__('Saved failed'));
        }
    }
}
