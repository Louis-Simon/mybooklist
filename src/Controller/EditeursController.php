<?php

namespace App\Controller;

use Cake\Log\Log;
use Cake\Network\Request;

class EditeursController extends AppController
{
    public function index(){

        $editeurs = $this->Editeurs->find('all');
        $this->set(compact('editeurs'));
    }

    public function display($id)
    {
        $this->loadModel('Genres');
        $this->loadModel('Auteurs');
        $editeur = $this->Editeurs->get($id, ['contain'=>['Editions' => ['Livres' => ['Genres', 'Auteurs']]]]);
        if($this->request->is(['auteur', 'put'])){
            $editeur = $this->Editions->patchEntity($editeur, $this->request->data);
        }
        $auteursId = array();
        $genresId = array();
        foreach($editeur->editions as $edition){
            foreach($edition->livre->auteurs as $auteur){
                array_push($auteursId, $auteur->idAuteur);
            }
            foreach($edition->livre->genres as $genre){
                array_push($genresId, $genre->idGenre);
            }
        }
        $auteursId = array_unique($auteursId);
        $genresId = array_unique($genresId);

        $parameters = array(
            'conditions' => array(
                'idAuteur IN' => $auteursId
            )
        );
        $editeur->auteurs = $this->Auteurs->find('all', $parameters);

        $parameters = array(
            'conditions' => array(
                'idGenre IN' => $genresId
            )
        );
        $editeur->genres = $this->Genres->find('all', $parameters);


        $this->set('editeur', $editeur);
    }
}

?>
