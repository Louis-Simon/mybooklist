<?php

namespace App\Controller;

use Cake\Log\Log;
use Cake\Network\Request;

class EditionsController extends AppController {

    public function index() {
        $newReleases = $this->getNewReleases();
        $topSuggestions = $this->getTopSuggestions();

        $this->set(compact('newReleases'));
        $this->set(compact('topSuggestions'));
    }

    public function all() {
        $parameters = array(
            'group' => 'Livre_idLivre');
        $allEditions = $this->Editions->find('all', $parameters);
        $allEditions = $this->getEditionsByUserPrefs($allEditions);
        $this->set(compact('allEditions'));
    }

    public function search($search) {
        $parameters = array(
            'conditions' => array(
                'sTitre LIKE' => ('%' . $search . '%')
            ), 'group' => 'Livre_idLivre'
        );

        $editions = $this->Editions->find('all', $parameters);

        $editions = $this->getEditionsByUserPrefs($editions);
        $this->set(compact('editions'));
    }

    private function getNewReleases() {
        $parameters = array('group' => 'Livre_idLivre', 'order' => ['dDateEdition' => 'DESC']);

        $editions = $this->Editions->find('all', $parameters);

        return $this->getEditionsByUserPrefs($editions);
    }

    private function getTopSuggestions() {

        $parameters = array('group' => 'Livre_idLivre', 'order' => ['Livres.fAvgRating' => 'DESC']);

        $editions = $this->Editions->find('all', $parameters)->contain(['Livres']);

        return $this->getEditionsByUserPrefs($editions);
    }

    private function getEditionsByUserPrefs($editions) {
        $finalEditions = array();
        foreach ($editions as $edition) {
            $parameters = array(
                'conditions' => array(
                    'Livre_idLivre' => $edition->Livre_idLivre
                )
            );
            $dateOriginale = $this->Editions->find('all', $parameters)->select([$editions->func()->min('dDateEdition')]);

            $langueId = $this->Auth->user('Langue_idLangue');
            $parameters = array(
                'conditions' => array(
                    'Livre_idLivre' => $edition->Livre_idLivre,
                    'OR' => [['Langue_idLangue' => $langueId ? $langueId : 2], ['dDateEdition' => $dateOriginale]]
                ), 'order' => ['dDateEdition' => 'DESC']
            );
            $livre = $this->Editions->find('all', $parameters)->contain(['Livres' => ['Auteurs']])->first();
            array_push($finalEditions, $livre);
        }
        return $finalEditions;
    }

    public function book($id) {
        $parameters = array(
            'conditions' => array(
                'idEdition' => $id
            )
        );

        $edition = $this->Editions->find('all', $parameters)->contain(['Livres' => ['Auteurs', 'Genres'], 'Editeurs', 'Lectures' => ['Users']])->first();
        $this->set(compact('edition'));
    }

}

?>
