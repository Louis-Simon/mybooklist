<?php
    $count = 0;
?>
<h1>Résultats de recherche</h1>
<table>
    <tr>
        <?php if(!empty($editions)){ foreach($editions as $edition){ ?>
            <td class="edition-box">
                <div class="edition-content">
                    <div class="caption">
                        <img height="300px" width="150px" src="<?php echo $edition->sImgPath;?>" style="cursor:pointer;" onclick="window.location = '/MyBookList/editions/book/<?php echo $edition->idEdition?>';"/>
                        <h4><?php echo $this->Html->link($edition->sTitre, ['controller' => 'Editions', 'action' => 'book', $edition->idEdition]);?></h4>
                            <?php if(!empty($edition->livre->auteurs)){foreach($edition->livre->auteurs as $auteur){?>
                        <?php echo $this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);?><br/>
                            <?php }}?>
                    </div>
                </div>
            </td>
            <?php
                if($count === 2){
                    echo '</tr><tr>';
                    $count = 0;
                }
                else{
                    $count++;
                }
            ?>
        <?php }} else { ?>
        <p class="no-record">No books found......
        </p>
        <?php } ?>
    </tr>
</table>
