<div id="container">
    <div id="editionId" style="display:none;"><?php echo $edition->idEdition; ?></div>
    <div class="section" style="height:auto;">
        <div id="bookTitle" style="text-align: center;">
            <h3><?php echo $edition->sTitre; ?></h3>
        </div>
        <div class="row">
                <img src="<?php echo $edition->sImgPath;?>" style="height: 300px; width: 175px; float:left; display:inline;"/>
            <div id="description" style="float:left; width:auto; height: 300px; margin-left: 2%;margin-top: 20px;">
                <?php
                    echo "<h5 style='display:inline;'>".__d('book', 'key_authors')."</h5>";
                    echo "<span>";
                    if(!empty($edition->livre->auteurs)){
                        $first = true;
                        foreach($edition->livre->auteurs as $auteur){
                            if($first){
                                echo $this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);
                                $first = false;
                            }
                            else{
                                echo ", ".$this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);
                            }
                        }
                    }
                    echo "</span>";

                ?>
                <br/>
                <h5 style='display:inline;'><?php echo __d('book', 'key_publisher'); ?></h5>
                <span><?php echo $this->Html->link(__d('editeur', $edition->editeur->sEditeurKey), ['controller' => 'Editeurs', 'action' => 'display', $edition->editeur->idEditeur]); ?></span>
                <br/>
                <h5 style='display:inline;'><?php echo __d('book', 'key_release'); ?></h5>
                <span><?php echo $edition->dDateEdition->i18nFormat('MM-dd-yyyy'); ?></span>
                <br/>
                <h5 style='display:inline;'><?php echo __d('book', 'key_genre'); ?></h5>
                <span><?php
                   $first = true;
                        foreach($edition->livre->genres as $genre){
                            if($first){
                                echo __d('book', $genre->sGenreKey);
                                $first = false;
                            }
                            else{
                                echo ", ".__d('book', $genre->sGenreKey);
                            }
                        }
                ?></span>
                <br/>
                <h5 style='display:inline;'><?php echo __d('book', 'key_isbn'); ?></h5>
                <span><?php echo $edition->sISBN; ?></span>
                <br/>
                <h5 style='display:inline;'><?php echo __d('book', 'key_score'); ?></h5>
                <span>
                    <?php
                        $fullStars = round($edition->livre->fAvgRating);
                        $emptyStars = 5 - $fullStars;
                        for($i = 0; $i < $fullStars; $i++){ ?>
                                <img src="/MyBookList/img/etoile_pleine.png" style="margin-left: 5px;"/>
                            <?php } ?>
                            <?php for($i = 0; $i < $emptyStars; $i++){ ?>
                                <img src="/MyBookList/img/etoile_vide.png" style="margin-left: 5px;"/>
                            <?php } ?>
                </span>
                <br/>
                <?php if(!(is_null($this->request->session()->read('Auth.User.idUsers')))){ ?>
                    <button id="btnAddToList" style="margin: 5px;" type="button"><?php echo __d('book', 'key_add'); ?></button>
                <?php }?>
                <!--<button style="margin: 5px;" type="button"><?php echo __d('book', 'key_view'); ?></button>-->

            </div>
        </div>
    </div>

    <div class="row" style="height:auto;">
       <h3 style="margin-top: 20px;"><?php echo __d('book', 'key_summary')?></h3>
        <p><?php echo $edition->sResume; ?></p>
    </div>
    <div class="row" style="height:auto;">
        <h3 style="margin-top: 20px;"><?php echo __d('book', 'key_review')?></h3>
        <?php foreach($edition->lectures as $lecture){ ?>
            <div style="height:30px;">
                <h5 style="float:left; margin-bottom:0px;"><?php echo $lecture->user->username; ?></h5>
                <span style="float:right;">
                    <?php
                        $fullStars = round($lecture->iRating);
                        $emptyStars = 5 - $fullStars;
                        for($i = 0; $i < $fullStars; $i++){ ?>
                                <img src="/MyBookList/img/etoile_pleine.png" style="margin-left: 5px;"/>
                            <?php } ?>
                            <?php for($i = 0; $i < $emptyStars; $i++){ ?>
                                <img src="/MyBookList/img/etoile_vide.png" style="margin-left: 5px;"/>
                            <?php } ?>
                </span>
            </div>
            <p><?php echo $lecture->sCommentaire; ?></p>
        <?php } ?>

    </div>
</div>
<?= $this->Html->script('book.js') ?>



