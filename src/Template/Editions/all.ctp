<?php
    $count = 0;
?>
<div class="section" style="height:auto;">
    <h1><?php echo __d('edition', 'key_editions_all'); ?></h1>
    <table>
        <tr>
            <?php foreach($allEditions as $edition){ ?>
                <td class="edition-box">
                    <div class="edition-content">
                        <div class="caption">
                            <img height="300px" width="150px" src="<?php echo $edition->sImgPath;?>" style="cursor:pointer;" onclick="window.location = '/MyBookList/editions/book/<?php echo $edition->idEdition?>';" />
                            <h4><?php echo $this->Html->link($edition->sTitre, ['controller' => 'Editions', 'action' => 'book', $edition->idEdition]);?></h4>
                            <?php if(!empty($edition->livre->auteurs)){
                                $first = true;
                                foreach($edition->livre->auteurs as $auteur){
                                    if($first){
                                        echo $this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);
                                        $first = false;
                                    }
                                    else{
                                        echo ", ".$this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);;
                                    }
                                }
                            }?>
                        </div>
                    </div>
                </td>
                <?php
                    if($count === 2){
                        echo '</tr><tr>';
                        $count = 0;
                    }
                    else{
                        $count++;
                    }
                ?>
            <?php }?>
        </tr>
    </table>
</div>
