
<div id="container">
    <div id="newReleases" class="section" style="top:50px;">
        <div class="title">
            <?php echo $this->Html->image('/img/eye_black.png', [
                "class" => "title_icon"
            ]); ?>
            <h1><?= __('key_new_releases') ?></h1>
        </div>
        <div id="booksRow">
            <?php echo '<div style="width: '.(count($newReleases) * 215).'px;">'  ?>
                <?php foreach($newReleases as $newRelease){ ?>
                <div class="book">
                    <img src="<?php echo $newRelease->sImgPath;?>" style="cursor:pointer;" onclick="window.location = '/MyBookList/editions/book/<?php echo $newRelease->idEdition?>';"/>
                    <p class="elipsis title1"><?php echo $this->Html->link($newRelease->sTitre, ['controller' => 'Editions', 'action' => 'book', $newRelease->idEdition]);?></p>
                    <p class="elipsis title2"><?php
                            $first = true;
                            if(!empty($newRelease->livre->auteurs)){
                        foreach($newRelease->livre->auteurs as $auteur){
                        if($first){ ?>
                        <?php echo $this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);?>
                        <?php    $first = false;
                                    }
                                    else{ ?>
                        <?php echo $this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);?>
                        <?php }
                                }
                            }
                        ?></p>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div id="topSuggestions" class="section">
        <div class="title">
            <?php echo $this->Html->image('/img/star_black.png', [
                "class" => "title_icon"
            ]); ?>
            <h1><?= __('key_top_suggestions') ?></h1>
        </div>
        <div id="suggestionsCarousel" class="suggestionGallery">
            <?php foreach($topSuggestions as $topSuggestion){ ?>
            <div class="suggestion">
                <div class="book">
                    <img src="<?php echo $topSuggestion->sImgPath;?>" style="cursor:pointer;" onclick="window.location = '/MyBookList/editions/book/<?php echo $topSuggestion->idEdition?>';"/>
                    <p class="elipsis title1"><?php echo $this->Html->link($topSuggestion->sTitre, ['controller' => 'Editions', 'action' => 'book', $topSuggestion->idEdition]);?></p>
                    <p class="elipsis title2"><?php
                            $first = true;
                            if(!empty($topSuggestion->livre->auteurs)){
                        foreach($topSuggestion->livre->auteurs as $auteur){
                        if($first){ ?>
                        <?php echo $this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);?>
                        <?php    $first = false;
                                    }
                                    else{ ?>
                        <?php echo $this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);?>
                        <?php }
                                }
                            }
                        ?></p>
                </div>
                <div class="core">
                    <?php
                            $fullStars = round($topSuggestion->livre->fAvgRating);
                    $emptyStars = 5 - $fullStars;
                    ?>
                    <div class="title1">
                        Score :
                        <?php for($i = 0; $i < $fullStars; $i++){ ?>
                        <img src="/MyBookList/img/etoile_pleine.png" style="margin-left: 5px;"/>
                        <?php } ?>
                        <?php for($i = 0; $i < $emptyStars; $i++){ ?>
                        <img src="/MyBookList/img/etoile_vide.png" style="margin-left: 5px;"/>
                        <?php } ?>
                        <br/>
                        <p class="elipsis"><?php echo $topSuggestion->sResume;?></p>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    docReady(function () {
        var flky = new Flickity('.suggestionGallery', {
            cellSelector: '.suggestion',
            resize: false,
            draggable: false
        });
        $('.elipsis').dotdotdot({wrap: 'letter'});
    });
</script>
