<div id="container">
    <div class="section" style="height:auto;">
        <h1><?php echo $auteur->sName;?></h1>
        <div class="row">
            <div class="author-img">
                <img height="300px" width="150px" src="<?php echo $auteur->sImgPath;?>" />
            </div>
            <div style="float:left;">
                <p><?php echo __d('author', 'key_born')?><?php echo $auteur->dDateNaissance->i18nFormat('MM-dd-yyyy')?></p>
                <p><?php if($auteur->dDateMort != null){?>
                <?php echo __d('author', 'key_died')?><?php echo $auteur->dDateMort->i18nFormat('MM-dd-yyyy');}?></p>
                <p><?php echo __d('author', 'key_genres');
                    $first = true;
                    foreach($auteur->genres as $genre){
                        if($first){
                            echo __($genre->sGenreKey);
                            $first = false;
                        }
                        else{
                            echo ", ".__($genre->sGenreKey);
                        }
                    }?>
                    </p>
                <p><?php echo __d('author', 'key_avg_score')?><?php echo $auteur->fAvgAuteur;?></p>
                <p><?php if($auteur->sLienWeb != null){?>
                <?php echo __d('author', 'key_website')?>
                <a href="<?echo $auteur->sLienWeb;?>" target="_blank"><?php echo $auteur->sLienWeb;}?></a></p>
            </div>
        </div>
    </div>
    <div class="section" style="height:auto;">
        <h3><?php echo __d('author', 'key_bio')?></h3>
        <div class="row">
            <p><?php echo __d('bio' ,$auteur->sBioKey)?></p>
        </div>
    </div>

    <div class="section" style="height:auto;">
        <h3><?php echo __d('author', 'key_books')?></h3>
        <div id="suggestionsCarousel" class="suggestionGallery">
            <?php foreach($auteur->livres as $livre){ ?>
                <div class="suggestion">
                    <div class="book">
                        <img src="<?php echo $livre->edition->sImgPath;?>" style="cursor:pointer;" onclick="window.location = '/MyBookList/editions/book/<?php echo $livre->edition->idEdition; ?>';"/>
                        <p class="elipsis title1"><?php echo $this->Html->link($livre->edition->sTitre, ['controller' => 'Editions', 'action' => 'book', $livre->edition->idEdition]); ?></p>
                        <p class="elipsis title2"></p>
                    </div>
                    <div class="core">
                        <?php
                            $fullStars = round($livre->fAvgRating);
                            $emptyStars = 5 - $fullStars;
                        ?>
                        <div class="title1">
                            Score :
                            <?php for($i = 0; $i < $fullStars; $i++){ ?>
                                <img src="/MyBookList/img/etoile_pleine.png" style="margin-left: 5px;"/>
                            <?php } ?>
                            <?php for($i = 0; $i < $emptyStars; $i++){ ?>
                                <img src="/MyBookList/img/etoile_vide.png" style="margin-left: 5px;"/>
                            <?php } ?>
                            <br/>
                            <p class="elipsis"><?php echo $livre->edition->sResume; ?></p>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    docReady(function(){
        var flky = new Flickity( '.suggestionGallery', {
            cellSelector:'.suggestion',
            resize: false,
            draggable: false
        });
        $('.elipsis').dotdotdot({wrap: 'letter'});
    });
</script>

