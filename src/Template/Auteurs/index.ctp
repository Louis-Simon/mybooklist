<h1>Auteurs</h1>
<div class="row">
    <?php if(!empty($auteurs)): foreach($auteurs as $auteur):	?>
        <div class="post-box">
            <div class="post-content">
                <div class="caption">
                    <h4><a href="../auteurs/display/<?php echo $auteur->idAuteur?>"><?php echo $auteur->sName;?></a></h4>
                    <h6><?php echo $auteur->dDateNaissance->i18nFormat('MM-dd-yyyy')?> - <?php if($auteur->dDateMort != null){
                        echo $auteur->dDateMort->i18nFormat('MM-dd-yyyy');}?></h6>
                    <p><?php echo $auteur->sLienWeb?></p>
                </div>
            </div>
        </div>
    <?php endforeach; else:	?>
        <p class="no-record">No	autor(s)	found......</p>
    <?php endif;	?>
</div>
