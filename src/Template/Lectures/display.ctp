<div style="padding-left:5%; padding-right:5%;">
    <div id="all">
        <table border="1" style="width:100%">
            <tr>
                <td class="lecture-columns" style="width:1%;">#</td>
                <td class="lecture-columns"><?php echo __d('lecture', 'Title'); ?></td>
                <td class="lecture-columns"><?php echo __d('lecture', 'Authors'); ?></td>
                <td class="lecture-columns"><?php echo __d('lecture', 'Publisher'); ?></td>
                <td class="lecture-columns"><?php echo __d('lecture', 'Language'); ?></td>
                <td class="lecture-columns"><?php echo __d('lecture', 'State'); ?></td>
                <td class="lecture-columns"><?php echo __d('lecture', 'Score'); ?></td>
            </tr>
            <?php $index=1; ?>
            <?php foreach($lectures as $lecture){?>
                <tr>
                    <td class="lecture-columns"><?php echo ($index); ?></td>
                    <td class="lecture-columns"><?php echo $this->Html->link($lecture->edition->sTitre,
                        ['controller' => 'Editions', 'action' => 'book', $lecture->edition->idEdition]); ?>
                    </td>
                    <td class="lecture-columns"><?php foreach($lecture->edition->livre->auteurs as $auteur){
                        echo $this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);
                        echo ", ";
                     }?></td>
                    <td class="lecture-columns"><?php echo $this->Html->link(__d('editeur', $lecture->edition->editeur->sEditeurKey),
                        ['controller' => 'Editeurs', 'action' => 'display', $lecture->edition->editeur->idEditeur]); ?>
                    </td>
                    <td class="lecture-columns"><?php echo __d('type',  $lecture->edition->langue->sLangueKey); ?></td>
                    <td class="lecture-columns"><?php echo __d('type', $lecture->etat->sEtatKey);?></td>
                    <td class="lecture-columns"><?php echo $lecture->iRating; ?></td>
                </tr>
                <?php $index++;?>

            <?php }?>
        </table>
    </div>
</div>
