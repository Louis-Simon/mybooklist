<h1><?php echo __d('editeur', 'key_editeurs'); ?></h1>
<div class="row">
    <?php if(!empty($editeurs)): foreach($editeurs as $editeur):	?>
        <div class="post-box">
            <div class="post-content">
                <div class="caption">
                    <h4><a href="../editeurs/display/<?php echo $editeur->idEditeur?>"><?php echo __d('editeur', $editeur->sEditeurKey);?></a></h4>
                </div>
            </div>
        </div>
    <?php endforeach; else:	?>
        <p class="no-record"><?php echo __d('editeur', 'key_editeurs_none'); ?></p>
    <?php endif;	?>
</div>
