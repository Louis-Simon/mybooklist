<div style="padding:5em;">
    <h1><?php echo __d('editeurDisplay', $editeur->sEditeurKey)?></h1>
    <div class="row">
        <div class="editeur-img">
            <img width="100%" src="<?php echo $editeur->sImgPath;?>"/>
        </div>
        <div style="float:left;">
            <p><?php echo __d('editeurDisplay', 'key_genres');
                $first = true;
                foreach($editeur->genres as $genre){
                    if($first){
                        echo __($genre->sGenreKey);
                        $first = false;
                    }
                    else{
                        echo ", ";
                        echo __($genre->sGenreKey);
                    }
                }?>
                </p>
            <p><?php if($editeur->sLienWebKey != null){?>
            <?php echo __d('editeurDisplay', 'key_website')?>
            <a href="<?php echo __d('editeur', $editeur->sLienWebKey);?>" target="_blank"><?php echo __d('editeur', $editeur->sLienWebKey);}?></a></p>
        </div>
    </div>

    <h3><?php echo __d('editeurDisplay', 'key_authors')?></h3>
    <div class="content">
        <?php foreach($editeur->auteurs as $auteur){?>
            <div class="row">
                <div style="width:30%; display:inline; float:left;">
                    <a href="../../auteurs/display/<?php echo $auteur->idAuteur; ?>">
                        <img height="300px" width="150px" src="<?php echo $auteur->sImgPath;?>" />
                    </a>
                </div>
                <div style="width:35%; display:inline; float:left;">
                    <p><?php echo $this->Html->link($auteur->sName, ['controller' => 'Auteurs', 'action' => 'display', $auteur->idAuteur]);?></p>
                    <p><?php echo __d('author', 'key_born'); echo $auteur->dDateNaissance->i18nFormat('MM-dd-yyyy');?></p>
                    <p><?php echo __d('author', 'key_died'); if(!($auteur->dDateMort==null)){echo $auteur->dDateMort->i18nFormat('MM-dd-yyyy');}?></p>
                </div>
                <div style="width:35%; display:inline; float:left;">
                    <p><?php echo __d('author', 'key_website'); echo $auteur->sLienWeb; ?><br/><br/></p>
                    <p><?php echo __d('author', 'key_avg_score'); echo $auteur->fAvgAuteur; ?></p>
                </div>
                <hr>
            </div>
        <?php }?>
    </div>
</div>
