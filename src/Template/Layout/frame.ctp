<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
    <head>
    <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            MyBookList | <?= $this->fetch('title') ?>
        </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('myBookList.css') ?>
    <?= $this->Html->css('flickity.css') ?>
    <?= $this->Html->script('utils/DOM.js') ?>
    <?= $this->Html->script('utils/array_contains.js') ?>
    <?= $this->Html->script('flickity.pkgd.min.js') ?>
    <?= $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js') ?>
    <?= $this->Html->script('utils/jquery.dotdotdot.min.js') ?>
    <?= $this->Html->script('frame.js') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
        <script>
            $(document).ready(function () {
                $("#flip").click(function () {
                    $("#panel").slideToggle("slow");
                });
            });
        </script>
    </head>
    <body>
        <nav class="top-bar expanded" data-topbar role="navigation">
            <ul class="title-area large-3 medium-4 columns">
                <li class="name">
                    <h1><a href="">MyBookList | <?= $this->fetch('title') ?></a></h1>

                </li>
            </ul>
            <section class="top-bar-section">
                <ul class="right">
                    <?php
                        if(is_null($this->request->session()->read('Auth.User.username'))){
                            ?><li id="flip"><a>Login</a></li>        
                            <div id="panel" hidden="true">
                                <div class="users form">
                                    <fieldset>
                                        <legend><?=__('Please enter your username and password') ?></legend>
                                        <input id="txtUsername" type="text" placeholder="<?php echo __d('type','key_invite_username'); ?>" style="margin-top: 20px;">
                                        <input id="txtPassword" type="password" placeholder="<?php echo __d('type','key_invite_password'); ?>" style="margin-top: 20px; margin-bottom: 20px;">
                                    </fieldset>
                                    <button type="button" onclick="window.location = '/MyBookList/users/signup/';"><?php echo __('Signup'); ?></button>
                                    <button id="btnLogin" type="button"><?php echo __('Login'); ?></button>
                                    <h6 id="usernameError" style="color: red; display: none; margin-top: 15px;"><?php echo __('key_login_fail'); ?></h6>
                                </div>
                            </div>
                               
                            </li><?php

                        }
                        else{
                            ?><li><?=$this->Html->link('Logout', ['controller' => 'Users', 'action' => 'logout'])?></li><?php
                        }?>

                </ul>
            </section>
        </nav>

        <nav class="search-bar" data-topbar role="navigation">
            <section class="search-bar-section">
                <ul class="left">
                    <li><?= $this->Html->link(__('key_home'), ['controller' => 'Editions', 'action' => 'index']) ?></li>
                    <li><?= $this->Html->link(__('key_all_books'), ['controller' => 'Editions', 'action' => 'all']) ?></li>
                    <?php
                        if(!(is_null($this->request->session()->read('Auth.User.idUsers')))){ ?>
                    <li><?= $this->Html->link(__('key_my_list'), ['controller' => 'Lectures', 'action' => 'display', $this->request->session()->read('Auth.User.idUsers')]) ?></li>
                        <?php }
                    ?>
                </ul>

            </section>
            <section class="search-bar-section">
                <ul class="right" style="padding-right: 5rem;">
                    <li>
                        <input id="search" type="text"/>
                    </li>
                    <li>
                        <a id="btnSearch"><?php echo __('key_search') ?></a>
                    </li>
                    <script type="text/javascript">
                        $('#btnSearch').bind('click', function (e) {
                            var search = $('#search').val();
                            if (search) {
                                window.location = ('/MyBookList/editions/search/' + search);
                            } else{
                                window.location = ('/MyBookList/editions/all');
                            }
                        });
                    </script>
                </ul>
            </section>
        </nav>
    <?= $this->Flash->render() ?>
        <section class="container clearfix">
        <?= $this->fetch('content') ?>
        </section>
        <footer>
            <nav class="bottom-bar expanded" data-topbar role="navigation">
                <section class="bottom-bar-section">
                    <ul>
                        <li>
                            <h6><?php echo __('key_about') ?></h6>
                        </li>
                        <li>
                            <h6><?php echo __('key_FAQ') ?></h6>
                        </li>
                        <li>
                            <h6><?php echo __('key_support') ?></h6>
                        </li>
                        <li>
                            <h6><?php echo __('key_terms') ?></h6>
                        </li>
                    </ul>
                </section>
                <section class="bottom-bar-section">
                    <h6><?php echo __('key_copyright'); ?></h6>
                </section>
            </nav>
        </footer>
    </body>

</html>
