<div class="users form">
<?= $this->Flash->render('auth') ?>
<?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?= $this->Form->input('username', ['label' => __d('type', 'key_invite_username')]) ?>
        <?= $this->Form->input('password', ['label' => __d('type', 'key_invite_password')]) ?>
        <?= $this->Form->input('sEmail', ['label' => __d('type', 'key_invite_email')]) ?>
        <?= $this->Form->input('Sexe_idSexe', [
            'label' => __d('type', 'key_invite_sexe'),
            'options' => [1 => __d('type','key_sexes_homme'),
             2 => __d('type', 'key_sexes_femme'),   
             3 => __d('type', 'key_sexes_autre')]
        ]) ?>
        <?= $this->Form->input('Langue_idLangue', [
            'label' => __d('type', 'key_invite_langue'),
            'options' => [1 => __d('type','key_langues_fr'),
             2 => __d('type', 'key_langues_en'),
             3 => __d('type', 'key_langues_gr')]
        ]) ?>
   </fieldset>
<?= $this->Form->button(__('Submit')); ?>
<?= $this->Form->end() ?>
</div>
