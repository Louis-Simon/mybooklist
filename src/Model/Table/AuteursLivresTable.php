<?php
namespace App\Model\Table;

use App\Model\Entity\AuteursLivre;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AuteursLivres Model
 *
 */
class AuteursLivresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('auteurs_livres');
        $this->displayField('idAuteurLivre');
        $this->primaryKey('idAuteurLivre');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idAuteurLivre')
            ->allowEmpty('idAuteurLivre', 'create');

        $validator
            ->integer('Livre_idLivre')
            ->requirePresence('Livre_idLivre', 'create')
            ->notEmpty('Livre_idLivre');

        $validator
            ->integer('Auteur_idAuteur')
            ->requirePresence('Auteur_idAuteur', 'create')
            ->notEmpty('Auteur_idAuteur');

        return $validator;
    }
}
