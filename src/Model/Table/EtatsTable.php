<?php
namespace App\Model\Table;

use App\Model\Entity\Etat;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Etats Model
 *
 */
class EtatsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('etats');
        $this->displayField('idEtat');
        $this->primaryKey('idEtat');

        $this->hasMany('Lectures', [
            'foreignKey' => 'Etat_idEtat',
            'dependent' => false
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idEtat')
            ->allowEmpty('idEtat', 'create');

        $validator
            ->requirePresence('sEtatKey', 'create')
            ->notEmpty('sEtatKey')
            ->add('sEtatKey', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['sEtatKey']));
        return $rules;
    }
}
