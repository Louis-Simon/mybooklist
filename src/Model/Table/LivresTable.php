<?php
namespace App\Model\Table;

use App\Model\Entity\Livre;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Livres Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Auteurs
 * @property \Cake\ORM\Association\BelongsToMany $Genres
 */
class LivresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('livres');
        $this->displayField('idLivre');
        $this->primaryKey('idLivre');

        $this->belongsToMany('Auteurs', [
            'foreignKey' => 'Livre_idLivre',
            'targetForeignKey' => 'Auteur_idAuteur',
            'joinTable' => 'auteurs_livres'
        ]);
        $this->belongsToMany('Genres', [
            'foreignKey' => 'Livre_idLivre',
            'targetForeignKey' => 'Genre_idGenre',
            'joinTable' => 'genres_livres'
        ]);
        $this->hasMany('Editions', [
            'foreignKey' => 'Livre_idLivre',
            'dependent' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idLivre')
            ->allowEmpty('idLivre', 'create');

        $validator
            ->integer('Serie_idSerie')
            ->allowEmpty('Serie_idSerie');

        $validator
            ->decimal('fAvgRating')
            ->allowEmpty('fAvgRating');

        return $validator;
    }
}
