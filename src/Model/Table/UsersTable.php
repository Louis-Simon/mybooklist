<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Membres Model
 *
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('username');
        $this->primaryKey('idUsers');
       
        $this->belongsTo('Langues', [
            'className' => 'Langues',
            'foreignKey' => 'Langue_idLangue',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {      
        return $validator
            ->add('Sexe_idSexe', 'inList', [
                'rule' => ['inList', [1, 2, 3]],
                'message' => __('keys_gender_invalid')
            ])
            ->notEmpty('username', __('keys_username_required'))
            ->notEmpty('password', __('keys_password_required'))
            ->notEmpty('sEmail', __('keys_email_required'));
    }
}
