<?php
namespace App\Model\Table;

use App\Model\Entity\Auteur;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Auteurs Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Livres
 */
class AuteursTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('auteurs');
        $this->displayField('idAuteur');
        $this->primaryKey('idAuteur');

        $this->belongsToMany('Livres', [
            'foreignKey' => 'Auteur_idAuteur',
            'targetForeignKey' => 'Livre_idLivre',
            'joinTable' => 'auteurs_livres'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idAuteur')
            ->allowEmpty('idAuteur', 'create');

        $validator
            ->requirePresence('sName', 'create')
            ->notEmpty('sName');

        $validator
            ->date('dDateNaissance')
            ->requirePresence('dDateNaissance', 'create')
            ->notEmpty('dDateNaissance');

        $validator
            ->date('dDateMort')
            ->allowEmpty('dDateMort');

        $validator
            ->allowEmpty('sLienWeb');

        $validator
            ->allowEmpty('sBioKey')
            ->add('sBioKey', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->decimal('fAvgAuteur')
            ->allowEmpty('fAvgAuteur');

        $validator
            ->allowEmpty('sImgPath');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['sBioKey']));
        return $rules;
    }
}
