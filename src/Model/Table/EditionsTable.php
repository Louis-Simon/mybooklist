<?php
namespace App\Model\Table;

use App\Model\Entity\Edition;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Editions Model
 *
 */
class EditionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('editions');
        $this->displayField('idEdition');
        $this->primaryKey('idEdition');

        $this->belongsTo('Livres', [
            'className' => 'Livres',
            'foreignKey' => 'Livre_idLivre',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Langues', [
            'className' => 'Langues',
            'foreignKey' => 'Langue_idLangue',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Editeurs', [
            'className' => 'Editeurs',
            'foreignKey' => 'Editeur_idEditeur',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Formats', [
            'className' => 'Formats',
            'foreignKey' => 'Format_idFormat',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Lectures', [
            'foreignKey' => 'Editions_idEdition'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idEdition')
            ->allowEmpty('idEdition', 'create');

        $validator
            ->integer('Livre_idLivre')
            ->requirePresence('Livre_idLivre', 'create')
            ->notEmpty('Livre_idLivre');

        $validator
            ->integer('Langue_idLangue')
            ->requirePresence('Langue_idLangue', 'create')
            ->notEmpty('Langue_idLangue');

        $validator
            ->integer('Format_idFormat')
            ->requirePresence('Format_idFormat', 'create')
            ->notEmpty('Format_idFormat');

        $validator
            ->integer('Editeur_idEditeur')
            ->requirePresence('Editeur_idEditeur', 'create')
            ->notEmpty('Editeur_idEditeur');

        $validator
            ->requirePresence('sTitre', 'create')
            ->notEmpty('sTitre');

        $validator
            ->allowEmpty('sISBN')
            ->add('sISBN', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('sImgPath');

        $validator
            ->date('dDateEdition')
            ->allowEmpty('dDateEdition');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['sISBN']));
        return $rules;
    }
}
