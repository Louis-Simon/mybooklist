<?php
namespace App\Model\Table;

use App\Model\Entity\Langue;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Langues Model
 *
 */
class LanguesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('langues');
        $this->displayField('idLangue');
        $this->primaryKey('idLangue');


        $this->hasMany('Editions', [
            'foreignKey' => 'Langue_idLangue',
            'dependent' => false
        ]);

        $this->hasMany('Users', [
            'foreignKey' => 'Langue_idLangue',
            'dependent' => false
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idLangue')
            ->allowEmpty('idLangue', 'create');

        $validator
            ->requirePresence('sLangueKey', 'create')
            ->notEmpty('sLangueKey')
            ->add('sLangueKey', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['sLangueKey']));
        return $rules;
    }
}
