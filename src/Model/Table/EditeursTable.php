<?php
namespace App\Model\Table;

use App\Model\Entity\Editeur;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Editeurs Model
 *
 */
class EditeursTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('editeurs');
        $this->displayField('idEditeur');
        $this->primaryKey('idEditeur');

        $this->hasMany('Editions', [
            'foreignKey' => 'Editeur_idEditeur',
            'dependent' => false
        ]);


    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idEditeur')
            ->allowEmpty('idEditeur', 'create');

        $validator
            ->requirePresence('sEditeurKey', 'create')
            ->notEmpty('sEditeurKey')
            ->add('sEditeurKey', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('sLienWebKey')
            ->add('sLienWebKey', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('sImgPath');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['sEditeurKey']));
        $rules->add($rules->isUnique(['sLienWebKey']));
        return $rules;
    }
}
