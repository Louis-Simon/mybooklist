<?php
namespace App\Model\Table;

use App\Model\Entity\GenresLivre;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GenresLivres Model
 *
 */
class GenresLivresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('genres_livres');
        $this->displayField('idGenreLivre');
        $this->primaryKey('idGenreLivre');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idGenreLivre')
            ->allowEmpty('idGenreLivre', 'create');

        $validator
            ->integer('Genre_idGenre')
            ->requirePresence('Genre_idGenre', 'create')
            ->notEmpty('Genre_idGenre');

        $validator
            ->integer('Livre_idLivre')
            ->requirePresence('Livre_idLivre', 'create')
            ->notEmpty('Livre_idLivre');

        return $validator;
    }
}
