<?php
namespace App\Model\Table;

use App\Model\Entity\Format;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Formats Model
 *
 */
class FormatsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('formats');
        $this->displayField('idFormat');
        $this->primaryKey('idFormat');

        $this->hasMany('Editions', [
            'foreignKey' => 'Editeur_idEditeur',
            'dependent' => false
        ]);


    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idFormat')
            ->allowEmpty('idFormat', 'create');

        $validator
            ->requirePresence('sFormatKey', 'create')
            ->notEmpty('sFormatKey');

        return $validator;
    }
}
