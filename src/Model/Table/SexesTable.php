<?php
namespace App\Model\Table;

use App\Model\Entity\Sex;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Sexes Model
 *
 */
class SexesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('sexes');
        $this->displayField('idSexe');
        $this->primaryKey('idSexe');

        $this->hasMany('Users', [
            'foreignKey' => 'Sexe_idSexe',
            'dependent' => false
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idSexe')
            ->allowEmpty('idSexe', 'create');

        $validator
            ->requirePresence('sSexeKey', 'create')
            ->notEmpty('sSexeKey')
            ->add('sSexeKey', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['sSexeKey']));
        return $rules;
    }
}
