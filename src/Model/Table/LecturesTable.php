<?php
namespace App\Model\Table;

use App\Model\Entity\Lecture;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Lectures Model
 *
 */
class LecturesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lectures');
        $this->displayField('idLecture');
        $this->primaryKey('idLecture');

        $this->belongsTo('Editions', [
            'className' => 'Editions',
            'foreignKey' => 'Editions_idEdition',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Etats', [
            'className' => 'Etats',
            'foreignKey' => 'Etats_idEtat',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'Users_idUsers',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('idLecture')
            ->allowEmpty('idLecture', 'create');

        $validator
            ->integer('Edition_idEdition')
            ->requirePresence('Edition_idEdition', 'create')
            ->notEmpty('Edition_idEdition');

        $validator
            ->integer('Etat_idEtat')
            ->requirePresence('Etat_idEtat', 'create')
            ->notEmpty('Etat_idEtat');

        $validator
            ->integer('Membre_idMembre')
            ->requirePresence('Membre_idMembre', 'create')
            ->notEmpty('Membre_idMembre');

        $validator
            ->allowEmpty('sCommentaire');

        $validator
            ->integer('iRating')
            ->allowEmpty('iRating');

        $validator
            ->date('dDateFin')
            ->allowEmpty('dDateFin');

        $validator
            ->date('dDateDebut')
            ->allowEmpty('dDateDebut');

        return $validator;
    }
}
