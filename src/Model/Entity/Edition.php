<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Edition Entity.
 *
 * @property int $idEdition
 * @property int $Livre_idLivre
 * @property int $Langue_idLangue
 * @property int $Format_idFormat
 * @property int $Editeur_idEditeur
 * @property string $sTitre
 * @property string $sISBN
 * @property string $sImgPath
 * @property \Cake\I18n\Time $dDateEdition
 */
class Edition extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'idEdition' => false,
    ];
}
