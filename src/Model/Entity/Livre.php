<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Livre Entity.
 *
 * @property int $idLivre
 * @property int $Serie_idSerie
 * @property float $fAvgRating
 * @property \App\Model\Entity\Auteur[] $auteurs
 * @property \App\Model\Entity\Genre[] $genres
 */
class Livre extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'idLivre' => false,
    ];
}
