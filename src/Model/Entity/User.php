<?php
namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity.
 *
 * @property int $idUsers
 * @property int $Sexe_idSexe
 * @property string $username
 * @property string $password
 * @property string $sEmail
 * @property \Cake\I18n\Time $dDateNaissance
 * @property string $sImgPath
 */
class User extends Entity
{
    protected $_accessible = [
        '*' => true,
        'idUsers' => false
    ];


    protected function _setPassword($password) {
        return (new DefaultPasswordHasher)->hash($password);
    }
}
?>
