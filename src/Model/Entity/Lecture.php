<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Lecture Entity.
 *
 * @property int $idLecture
 * @property int $Edition_idEdition
 * @property int $Etat_idEtat
 * @property int $Membre_idMembre
 * @property string $sCommentaire
 * @property int $iRating
 * @property \Cake\I18n\Time $dDateFin
 * @property \Cake\I18n\Time $dDateDebut
 */
class Lecture extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'idLecture' => false,
    ];
}
