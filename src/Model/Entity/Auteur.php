<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Auteur Entity.
 *
 * @property int $idAuteur
 * @property string $sName
 * @property \Cake\I18n\Time $dDateNaissance
 * @property \Cake\I18n\Time $dDateMort
 * @property string $sLienWeb
 * @property string $sBioKey
 * @property float $fAvgAuteur
 * @property string $sImgPath
 * @property \App\Model\Entity\Livre[] $livres
 */
class Auteur extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'idAuteur' => false,
    ];
}
