var DOM = dom();

function dom(){
    var _proto = dom.prototype;

    _proto.queryFirstElement = function queryFirstElement(selector, element){
        if(!element){
            element = document;
        }

        return element.querySelector(selector);
    };

    _proto.queryLastElement = function queryLastElement(selector, element){
        if(!element){
            element = document;
        }

        var allElements = element.querySelectorAll(selector);
        return allElements[allElements.length - 1];
    };

    _proto.queryAllElements = function queryLastElement(selector, element){
        if(!element){
            element = document;
        }

        return nodeListToArray(element.querySelectorAll(selector));
    };

    function nodeListToArray(nodeList){
        var arr = [];
        for(var i = nodeList.length; i--; arr.unshift(nodeList[i]));
        return arr;
    }

    return Object.create(_proto);
}
