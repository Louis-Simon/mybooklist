document.addEventListener("DOMContentLoaded", function (event) {
    DOM.queryFirstElement('#btnLogin').addEventListener('click', function () {
        var user = {
            username: DOM.queryFirstElement('#txtUsername').value,
            password: DOM.queryFirstElement('#txtPassword').value
        };

        var request = new XMLHttpRequest();
        request.open('POST', '/MyBookList/users/login', true);
        request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    window.location = '/MyBookList/'
                }
                else {
                    DOM.queryFirstElement('#usernameError').style.display = '';
                }
            }
        }
        request.send(JSON.stringify(user));
    });
});

