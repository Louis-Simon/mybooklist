DOM.queryFirstElement('#btnAddToList').addEventListener('click', function(){
    var id = DOM.queryFirstElement('#editionId').textContent;
    var request = new XMLHttpRequest();
    request.open('POST', '/MyBookList/lectures/add/' + id, true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    alert('Le livre a été ajouté avec succès');
                }
                else {
                    alert('L\'ajout du livre a échoué');
                }
            }
        }
    var response = request.send();
});
